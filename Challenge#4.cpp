#include <vector>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <iomanip>
using namespace std;
	void random(vector<int>&array);//function that random number in vector and vector size
	int sum(vector<int>&array);//function that sum all number in vector
	void maximum(vector<int>&array);//function that find highest value
	void minimum(vector<int>&array);//function that find lowest value
	float mean(vector<int>&array);//function that sum all number in vector and divided by array size
	void median(vector<int>&array);//function that use vector and divided by 2 to located middle vector size and output value
	void mode(vector<int>&array);//function that counter the most numbers  
	void even(vector<int>&array);//function that counter even numbers in vector
	void odd(vector<int>&array);//function that counter odd numbers in vector
	void sort(vector<int>&array);//function that sort numbers in vector from lowest value to highest value

int main(){
vector<int> array;
cout << "The number of element:"; random(array) ;
cout << "\nThe sum of all element: " << sum(array);
cout << "\nThe highest value: " ; maximum(array);
cout << "\nThe lowest value: " ; minimum(array);
cout <<"\nThe mean value: " << mean(array);
cout << "\nThe median value: " ; median(array);
cout << "\nThe mode value: " ; mode(array);
cout << "\nThe number of even number: " ; even(array);
cout << "\nThe number of odd number: " ; odd(array);
cout << "\nThe values output in order from lowest to highest:\n" ; sort(array);
return 0;
}
void random(vector<int>&array){//O(n)
	int j,i;
	srand(time(0));
 	for(i=0;i<=rand()%100+50;i++){
  		j=rand()%101;
  			array.push_back(j);
  
	}cout << array.size();
}
int sum(vector<int>&array){//O(n)
	int i,all=0;
	for(i=0;i<=array.size()-1;i++){
		all=all+array[i];
	}
	return all;
}
void maximum(vector<int>&array){//O(n)
	int max=array[0]; int i;
	for(i=0;i<=array.size()-1;i++){
		if(max < array.at(i)){//O(1)
		 max = array.at(i);
		}
	}cout << max;
}
void minimum(vector<int>&array){//O(n)
	int min=array[0]; int i;
	for(i=0;i<=array.size()-1;i++){
		if(min > array.at(i)){//O(1)
		 min = array.at(i);
		}
	}cout << min;
}
float mean(vector<int>&array){//O(n)
	int i;
	float sum=0;
		for(int i=0;i<=array.size()-1;i++){
		sum=sum+array[i];
	}
	return sum/array.size(); 
}
void median(vector<int>&array){//O(n^2)
	int swap,hold,x=0,y=0;
	float sum=0.5;
	do{
			swap=0;
		for(int j=0;j<array.size()-1;j++)
		{	
			if(array.at(j)>array.at(j+1)){//O(1)
				hold = array.at(j);
				array.at(j)=array.at(j+1);
				array.at(j+1)=hold;
				swap=1	;
			}
		}		
	}while(swap==1);
	if(array.size()%2!=0){//O(1)
		x=(array.size()/2);
		cout << array[x];
	}else if(array.size()%2==0){//O(1)
		x=(array.size()/2),y=(array.size()/2)-1;
		if(array.at(x)==array.at(y)){//O(1)
			sum=(array.at(x)+array.at(y))/2;
		cout << sum << " ";
		}else{//O(1)
			sum=sum+(array.at(x)+array.at(y))/2;
		cout << sum << " ";
		}
	}
}
void mode(vector<int>&array){//O(n^2)
	int freq[101]={},max,mode=0;
	for(int i=0;i<=array.size()-1; i++){
		freq[array.at(i)]++;
	}
	max=freq[0];
	for(int i=0;i<=100;i++){
		if(freq[i]>max){//O(1)
			max=freq[i];
			mode=i;
			
		}
	}
	cout << mode;
}
void even(vector<int>&array){//O(n)
	int count=0;
	for(int i=0;i<=array.size()-1;i++){
		if(array.at(i)%2==0){//O(1)
			count++;
		}
	}cout << count;
}
void odd(vector<int>&array){//O(n)
	int count=0;
	for(int i=0;i<=array.size()-1;i++){
		if(array.at(i)%2!=0){//O(1)
			count++;
		}
	}cout << count;
}
void sort(vector<int>&array)//O(n^2)
	int swap,hold;
	do{//O(n^2)
			swap=0;
		for(int j=0;j<array.size()-1;j++){
			if(array.at(j)>array.at(j+1)){//O(1)
				hold = array.at(j);
				array.at(j)=array.at(j+1);
				array.at(j+1)=hold;
				swap=1	;
		}
			}
	}while(swap==1);
	for(int i=0;i<=array.size()-1;i++){//O(n)
		cout << setw(6) << array.at(i);
		if(i%10==9){//O(1)
			cout << endl;
		}
	}
}
